package dto;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Empleados {
	
	private static Connection conexion;
	private static String tabla = "Empleados";
	private static String bd;
	
	private String DNI = "";
	private String nombre = "";
	private String apellidos = "";
	private int departamento = 0;
	
	
	//CONSTRUCTORES
	public Empleados(Connection conexion, String bd) {
		this.conexion = conexion;
		this.bd = bd;
		createTable();
	}
	
	public Empleados(Connection conexion, String bd, String DNI, String nombre, String apellidos, int departamento) {
		this.conexion = conexion;
		this.bd = bd;
		createTable();
		this.DNI = DNI;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.departamento = departamento;
		crearEmpleado(DNI, nombre, apellidos, departamento);	
	}
	
	public Empleados(Connection conexion, String bd, String DNI, String nombre, String apellidos) {
		this.conexion = conexion;
		this.bd = bd;
		createTable();
		this.DNI = DNI;
		this.nombre = nombre;
		this.apellidos = apellidos;
		crearEmpleado(DNI, nombre, apellidos);	
	}
	
	public Empleados(Connection conexion, String bd, String DNI) {
		this.conexion = conexion;
		this.bd = bd;
		createTable();
		this.DNI = DNI;
		crearEmpleado(DNI);	
	}
	
	//CREACI� TAULA
	public static void createTable() {
	
		try {
			String Querydb = "USE " + bd + ";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE IF NOT EXISTS "+ tabla +
					" (DNI VARCHAR(8) PRIMARY KEY, nombre NVARCHAR(100), "
					+ "apellidos NVARCHAR(255), departamento INT,"
					+ " FOREIGN KEY(departamento) REFERENCES Departamentos(codigo));";
			
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Taula creada");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("Error creando tabla");
		}
		
	}
	
	
	//INSERCI� DE EMPLEATS
	public static void crearEmpleado(String DNI, String nombre, String apellidos, int departamento) {
		System.out.println("Articulos.crearArticulo()dninombreapellidodepartamento");
			try {
				String Querydb = "USE "+bd+";";
				Statement stdb = conexion.createStatement();
				stdb.executeUpdate(Querydb);
				
				String Query = "INSERT INTO " + tabla + " (DNI, nombre, apellidos, departamento) VALUE ("
						+ "\" "+ DNI +"\",\" "+nombre+"\",\" "+apellidos+"\",\" "+departamento+"\");";
				
				Statement st = conexion.createStatement();
				st.execute(Query);
				
				System.out.println("Datos introducidos");
			} catch (SQLException e) {
				System.out.println("No s'ha pogut introduir");
			}
	}

	public static void crearEmpleado(String DNI, String nombre, String apellidos) {
		System.out.println("Articulos.crearArticulo()dninombreapellidodepartamento");
			try {
				String Querydb = "USE "+bd+";";
				Statement stdb = conexion.createStatement();
				stdb.executeUpdate(Querydb);
				
				String Query = "INSERT INTO " + tabla + " (DNI, nombre, apellidos, departamento) VALUE ("
						+ "\" "+ DNI +"\",\" "+nombre+"\",\" "+apellidos+"\");";
				
				Statement st = conexion.createStatement();
				st.execute(Query);
				
				System.out.println("Datos introducidos");
			} catch (SQLException e) {
				System.out.println("No s'ha pogut introduir");
			}
	}
	
	public static void crearEmpleado(String DNI) {
		System.out.println("Articulos.crearArticulo()dninombreapellidodepartamento");
			try {
				String Querydb = "USE "+bd+";";
				Statement stdb = conexion.createStatement();
				stdb.executeUpdate(Querydb);
				
				String Query = "INSERT INTO " + tabla + " (DNI, nombre, apellidos, departamento) VALUE ("
						+ "\" "+ DNI +"\");";
				
				Statement st = conexion.createStatement();
				st.execute(Query);
				
				System.out.println("Datos introducidos");
			} catch (SQLException e) {
				System.out.println("No s'ha pogut introduir");
			}
	}
	
	
	//OBTENER ARTICUO
	public static void obtenerArticulos() {
		
		try {
			String Querydb = "USE "+ bd +";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "SELECT * FROM " + tabla;
			Statement st = conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
			
			while(resultSet.next()) {
				System.out.println("-------------\nCodigo:	" + resultSet.getString("DNI") +"\nNombre:" + resultSet.getString("nombre")+"\nApellidos:" + resultSet.getString("apellidos") +"\nFabricante:" + resultSet.getInt("departamento"));
			}			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error");
		}		
	}
	
	
	//MODIFICACIO VALOR NOM
	public void cambiarNombre(String nombreNuevo, String DNI) {
		try {
			String Querydb = "USE "+bd+";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "UPDATE " + tabla + " SET nombre = \""+nombreNuevo+"\" WHERE  DNI =\""+DNI+"\";";
	
			Statement st = conexion.createStatement();
			st.execute(Query);
			
			System.out.println("Datos introducidos");
		} catch (SQLException e) {
			System.out.println("No s'ha pogut introduir");
		}
	}
	
	//CAMBIAR APELLIDOS
	public void cambiarApellidos(String ApellidoNuevo, String DNI) {
		try {
			String Querydb = "USE "+bd+";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "UPDATE " + tabla + " SET apellidos = \""+ApellidoNuevo+"\" WHERE  DNI =\""+DNI+"\";";
		
			
			Statement st = conexion.createStatement();
			st.execute(Query);
			
			System.out.println("Datos introducidos");
		} catch (SQLException e) {
			System.out.println("No s'ha pogut introduir");
		}
	}
	
	//CAMBIAR DEPARTAMENTO
	public void cambiarDepartamento(int departamento, String DNI) {
		try {
			String Querydb = "USE "+bd+";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "UPDATE " + tabla + " SET departamento = \""+departamento+"\" WHERE  DNI =\""+DNI+"\";";
		
			
			Statement st = conexion.createStatement();
			st.execute(Query);
			
			System.out.println("Datos introducidos");
		} catch (SQLException e) {
			System.out.println("No s'ha pogut introduir");
		}
	}
	
	
	//GET I SET

	public String getDNI() {
		return DNI;
	}


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
		cambiarNombre(nombre, this.DNI);
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
		cambiarApellidos(apellidos, this.DNI);
	}

	public int getDepartamento() {
		return departamento;
	}

	public void setDepartamento(int departamento) {
		this.departamento = departamento;
		cambiarDepartamento(departamento, this.DNI);
	}
	
	

}


