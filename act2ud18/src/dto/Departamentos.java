package dto;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Departamentos {
	
	private static Connection conexion;
	private static String tabla = "Departamentos";
	private static String bd;
	
	private int codigo = 0;
	private String nombre = "";
	private int presupuesto = 0;
	
	
	//CONSTRUCTORES
	public Departamentos(Connection conexion, String bd) {
		this.conexion = conexion;
		this.bd = bd;
		createTable();
	}
	
	public Departamentos(Connection conexion, String bd, String nombre) {
		this.conexion = conexion;
		this.bd = bd;
		createTable();
		this.nombre = nombre;
		crearDepartamento(nombre);	
		this.codigo = obtenerCodigo(nombre);
	}
	
	public Departamentos(Connection conexion, String bd, String nombre, int presupuesto) {
		this.conexion = conexion;
		this.bd = bd;
		createTable();
		this.nombre = nombre;
		this.presupuesto = presupuesto;
		crearDepartamento(nombre, presupuesto);	
		this.codigo = obtenerCodigo(nombre);
	}
	
	
	//CREACI� TAULA
	public static void createTable() {
	
		try {
			String Querydb = "USE " + bd + ";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE IF NOT EXISTS "+ tabla +
					" (codigo INT PRIMARY KEY AUTO_INCREMENT, nombre NVARCHAR(100), "
					+ "presupuesto INT);";
			
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Taula creada");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("Error creando tabla");
		}
		
	}
	
	
	//INSERCI� DE EMPLEATS
	public static void crearDepartamento(String nombre, int presupuesto) {
		System.out.println("Articulos.crearArticulo()codigonombrepresupuesto");
			try {
				String Querydb = "USE "+bd+";";
				Statement stdb = conexion.createStatement();
				stdb.executeUpdate(Querydb);
				
				String Query = "INSERT INTO " + tabla + " (nombre, presupuesto) VALUE (\""+nombre+"\",\""+presupuesto+"\");";
				
				Statement st = conexion.createStatement();
				st.execute(Query);
				
				System.out.println("Datos introducidos");
			} catch (SQLException e) {
				System.out.println("No s'ha pogut introduir");
			}
	}
	
	public static void crearDepartamento(String nombre) {
		System.out.println("Articulos.crearArticulo()codigonombre");
			try {
				String Querydb = "USE "+bd+";";
				Statement stdb = conexion.createStatement();
				stdb.executeUpdate(Querydb);
				
				String Query = "INSERT INTO " + tabla + " (nombre) VALUE (\""+nombre+"\");";
				
				Statement st = conexion.createStatement();
				st.execute(Query);
				
				System.out.println("Datos introducidos");
			} catch (SQLException e) {
				System.out.println("No s'ha pogut introduir");
			}
	}
	
	
	//OBTENER ARTICUO
	public static void obtenerDepartamentos() {
		
		try {
			String Querydb = "USE "+ bd +";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "SELECT * FROM " + tabla;
			Statement st = conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
			
			while(resultSet.next()) {
				System.out.println("-------------\nCodigo:	" + resultSet.getInt("codigo") +"\nNombre:" + resultSet.getString("nombre")+"\nPresupuesto:" + resultSet.getString("presupuesto"));
			}			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error");
		}		
	}
	
	
	//MODIFICACIO VALOR NOM
	public void cambiarNombre(String nombre, int codigo) {
		try {
			String Querydb = "USE "+bd+";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "UPDATE " + tabla + " SET nombre = \""+nombre+"\" WHERE  codigo =\""+codigo+"\";";
	
			Statement st = conexion.createStatement();
			st.execute(Query);
			
			System.out.println("Datos introducidos");
		} catch (SQLException e) {
			System.out.println("No s'ha pogut introduir");
		}
	}
	
	//CAMBIAR PRESUPUESTO
	public void cambiarDepartamento(int presupuesto, int codigo) {
		try {
			String Querydb = "USE "+bd+";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "UPDATE " + tabla + " SET presupuesto = \""+presupuesto+"\" WHERE  codigo =\""+codigo+"\";";
		
			
			Statement st = conexion.createStatement();
			st.execute(Query);
			
			System.out.println("Datos introducidos");
		} catch (SQLException e) {
			System.out.println("No s'ha pogut introduir");
		}
	}
	

	public static int obtenerCodigo(String nombre) {
		int codigo = 0;
		try {
			String Querydb = "USE "+bd+";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "Select codigo from " + tabla + " where nombre = \""+nombre+"\";";
		
			
			Statement st = conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
			
			codigo = resultSet.getInt("codigo");
			
			System.out.println("Datos introducidos");
		} catch (SQLException e) {
			System.out.println("No s'ha pogut trobar");
		}
		
		
		return codigo;
	}
	
	//GET I SET
	
	
	public int getCodigo() {
		return codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
		cambiarNombre(nombre, this.codigo);
	}

	public int getPresupuesto() {
		return presupuesto;
	}

	public void setPresupuesto(int presupuesto) {
		this.presupuesto = presupuesto;
		cambiarDepartamento(presupuesto, this.codigo);
	}
	
	

}


