package act1ud18App;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.InputMismatchException;
import java.util.Scanner;

import javax.swing.JOptionPane;

import dto.Departamentos;
import dto.Empleados;
import dto.Departamentos;


public class act1ud18App {

	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		final String bd = "act2";
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection conexion = DriverManager.getConnection("jdbc:mysql://192.168.1.134:3306?"
					+ "useTimezone=true&serverTimezone=UTC","remote","12345Remote<3!");
			System.out.println("Benvolgut a la tenda de inform�tica");
			
			createDB(bd, conexion); 
			
			/*CREACI� DE 5 DEPARTAMENTS*/
			
			//departament : nom
			Departamentos departament = new Departamentos(conexion, bd, "Marqueting");
			Departamentos departament1 = new Departamentos(conexion, bd, "Ciberseguretat");
			//departament: nom + presupuesto
			Departamentos departament2 = new Departamentos(conexion, bd, "Informatica", 100);
			Departamentos departament3 = new Departamentos(conexion, bd, "Contabilitat", 200);
			Departamentos departament4 = new Departamentos(conexion, bd, "Executiu", 300);
			
			/*CREACI� DE 5 EMPLEATS*/
			//dni
			Empleados empleat = new Empleados(conexion, bd, "48832182");
			
			//dni+nombre+apellidos
			Empleados empleat1 = new Empleados(conexion, bd, "32212203", "Luisa", "Gimenez");
			
			//dni+nombre+apellidos+departamento
			Empleados empleat2 = new Empleados(conexion, bd, "83992019", "Juan", "Martillos", 3);
			
			
			/*MOSTRAR*/
			Departamentos departamentoM = new Departamentos(conexion, bd);
			departamentoM.obtenerDepartamentos();
			
			Empleados empleadoM = new Empleados(conexion, bd);
			empleadoM.obtenerArticulos();
			
		} catch (SQLException | ClassNotFoundException e) {
			System.out.println("No se ha podido conectar con la base de datos");
			System.out.println(e);
		}

	}
	
	
	
	
	// CREACI� DE LA BASE DE DADES
	public static void createDB(String name, Connection conexion) {
		try {
			String Query = "CREATE DATABASE " + name;
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			JOptionPane.showMessageDialog(null, "Se ha creado la bd "+ name +" exitosamente");
		} catch (SQLException ex) {
			System.out.println("Error al crear database");
		}
	}


	
	

}
