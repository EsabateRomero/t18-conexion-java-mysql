package act1ud18App;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.InputMismatchException;
import java.util.Scanner;

import javax.swing.JOptionPane;

import dto.Articulos;
import dto.Fabricantes;


public class act1ud18App {

	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		final String bd = "act1";
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection conexion = DriverManager.getConnection("jdbc:mysql://192.168.1.134:3306?"
					+ "useTimezone=true&serverTimezone=UTC","remote","12345Remote<3!");
			System.out.println("Benvolgut a la tenda de inform�tica");
			
			createDB(bd, conexion); 
			boolean sortir = false;
			
			while(!sortir) {
				
				switch (menu(teclat)) {
				case 1:
					//1. CREAR FABRICANTE
					introducirFabricante(teclat, conexion, bd);
					break;
				case 2:
					//2. CREAR ART�CULO
					introducirArticulo(teclat, conexion, bd);
					break;	
				case 3:
					//3. VISUALIZAR FABRICANTES
					Fabricantes fabricante = new Fabricantes(conexion, bd);
					fabricante.obtenerFabricantes();
					break;
				case 4:
					//VISUALTZAR ARTICLES
					Articulos articulo = new Articulos(conexion, bd);
					articulo.obtenerArticulos();
					break;
				}
				
			}
				
			
		} catch (SQLException | ClassNotFoundException e) {
			System.out.println("No se ha podido conectar con la base de datos");
			System.out.println(e);
		}

	}
	
	
	
	//INTRODUCIR INT
	public static int introduirInt(Scanner teclat) {
		int Int = 0;
		boolean nombreValid = false;
		while(!nombreValid) {
			try {
				System.out.println("Introdueix el valor: ");
				Int = teclat.nextInt();
				nombreValid = true;
			} catch (InputMismatchException noApte) {
				System.out.println("Heu d'introduir un int");
			}
		}
		return Int;
	}
	
	//INTRODUCIR STRING
	public static String introduirString(Scanner teclat) {
		String string = "";
		boolean valorValid = false;
		while(!valorValid) {
			System.out.println("Introdueix el valor: ");
			try {
				
				string = teclat.next();
				
			} catch (InputMismatchException noApte) {
				System.out.println("Heu d'introduir un String");
			}
		}valorValid = true;
		return string;
	}
	
	
	//INTRODUCIR ARTICULO
	public static void introducirArticulo(Scanner teclat, Connection conexion, String bd) {
		Articulos articulo; 
		boolean salir = false, nombreB = false, precioB = false, fabricanteB = false;
		String nombre = ""; int precio = 0, fabricante = 0; Fabricantes fabricant = null;
		System.out.println("Creaci� d'un producte\n--------------------------------\n	- "
				+ "Afegir el nom (si/no)");
		
		
			nombreB = true;
			System.out.println("Introdueix el nom: ");
			nombre = teclat.next();
	
		
		System.out.println("Vol afegir el preu? (si/no)");
		if(opcioBinaria(teclat)) {
			precioB = true;
			System.out.println("Introdueix el valor: ");
			precio = teclat.nextInt();
		}
		
		if(precioB) {
			
		System.out.println("Vol afegir el fabricant? (si/no)");
			if(opcioBinaria(teclat)) {
				fabricanteB = true;
				fabricante = teclat.nextInt();
						
			}
		}
	
		
		if(!nombreB&&!precioB&&!fabricanteB)
			articulo = new Articulos(conexion, bd);
		if(nombreB&&!precioB&&!fabricanteB)
			articulo = new Articulos(conexion, bd, nombre);
		if(nombreB&&precioB&&!fabricanteB)
			articulo = new Articulos(conexion, bd, nombre, precio);
		if(nombreB&&precioB&&fabricanteB)
			articulo = new Articulos(conexion, bd, nombre, precio, fabricante);
		
	}
	
	
	//OPCIO SI/NO --> SI = TRUE
	public static boolean opcioBinaria(Scanner teclat){
		boolean opcioValida = false;
		boolean opcio = false; String opcioS = "";
		while(!opcioValida) {
			System.out.println();
			opcioS = teclat.next();
			if(opcioS.equalsIgnoreCase("si")|opcioS.equalsIgnoreCase("no")) {
				opcioValida = true;	
				if(opcioS.equalsIgnoreCase("si"))
					opcio = true;
			}
				
		}
		
		
		return opcio;
	}
	
	
	//INTRODUIR FABRICANT
	public static void introducirFabricante(Scanner teclat, Connection conexion, String bd) {
		Fabricantes fabricante; 
		boolean salir = false;
		System.out.println("Nom del fabricant que voleu introduir");
		
		while(!salir) {
			
			String nombreFabricante = teclat.next();
			fabricante = new Fabricantes(conexion, bd, nombreFabricante);
			System.out.println("Voleu afegir un altre? Si/No");
			boolean opcioValida = false;
				while(!opcioValida) {
					String opcio = teclat.next();
					if(opcio.equalsIgnoreCase("si")||opcio.equalsIgnoreCase("no"))
						opcioValida = true;
					if(opcio.equalsIgnoreCase("si"))
						System.out.println("Introdueix-lo:");
					
				}
				
		}
		
	}
	
	
	//CREACI� DEL MEN� PEL SWITCH (SOLS ACCEPTAR VALORS DINS D'UN RANG CONCRET
	public static int menu(Scanner teclat) {
		boolean opcioValida = false;
		int opcio = 0;
		while(!opcioValida) {
			System.out.println("1. Introdu�r fabricant\n2. Introduir article\n3. Visualitzar fabricant\n4. Visualitzar article\n\n --> Tria una opci�:");
			opcio = teclat.nextInt();
			if(opcio>0&&opcio<5)
				opcioValida = true;
			else
				System.out.println("Valor no v�lid	-> Introdueix un altre valor:");
		}
		return opcio;
	}
	
	
	// CREACI� DE LA BASE DE DADES
	public static void createDB(String name, Connection conexion) {
		try {
			String Query = "CREATE DATABASE " + name;
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			JOptionPane.showMessageDialog(null, "Se ha creado la bd "+ name +" exitosamente");
		} catch (SQLException ex) {
			System.out.println("Error al crear database");
		}
	}


	
	

}
