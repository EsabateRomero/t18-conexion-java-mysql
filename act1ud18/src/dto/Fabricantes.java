package dto;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.cj.protocol.Resultset;

public class Fabricantes {
	
	private static Connection conexion;
	private static String tabla = "Fabricantes";
	private static String bd;
	
	private int codigo = 0;
	private String nombre = "";
	
	public Fabricantes(Connection conexion, String bd) {
		this.conexion = conexion;
		this.bd = bd;
		createTable();
		
	}
	
	public Fabricantes(Connection conexion, String bd, int codigoFabricante) {
		this.conexion = conexion;
		this.bd = bd;
		
	}
	
	public Fabricantes(Connection conexion, String bd, int codigoFabricante, String nombre) {
		this.conexion = conexion;
		this.bd = bd;
		
	}
	
	
	public Fabricantes(Connection conexion, String bd, String nombre) {
		this.conexion = conexion;
		this.bd = bd;
		createTable();
		this.nombre = nombre;
		crearFabricante(nombre);
	}
	
	public static void createTable() {
	
		try {
			String Querydb = "USE " + bd + ";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE "+ tabla +
					" (codigo INT PRIMARY KEY AUTO_INCREMENT, nombre VARCHAR(50));";
			
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Taula creada");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("Error creando tabla");
		}
		
		
	}
	

	public static Fabricantes obtenerFabricantePorCodigo(int codigoFabricante) {
		String nombre = "";
		try {
			String Querydb = "USE "+ bd +";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "SELECT * FROM " + tabla + "WHERE 'codigo' = '"+codigoFabricante+"';";
			Statement st = conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
			nombre = resultSet.getString("nombre");
			
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error");
			
		}
		
		Fabricantes fabricante = new Fabricantes(conexion, bd, codigoFabricante, nombre);
		return fabricante;
	}
	
	public static void crearFabricante(String nombre) {
		
		try {
		String Querydb = "USE "+bd+";";
		Statement stdb = conexion.createStatement();
		stdb.executeUpdate(Querydb);
		
		String Query = "INSERT INTO " + tabla + " (nombre) VALUE ("
				+ "\" "+ nombre +"\");";
		
		Statement st = conexion.createStatement();
		st.execute(Query);
		
		System.out.println("Datos introducidos");
		} catch (SQLException e) {
			System.out.println("No s'ha pogut introduir");
		}
	}
	
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public static void obtenerFabricantes() {
		
		try {
			String Querydb = "USE "+ bd +";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "SELECT * FROM " + tabla;
			Statement st = conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
			
			while(resultSet.next()) {
				System.out.println("-------------\nCodigo:	" + resultSet.getInt("codigo") +"	Nombre:" + resultSet.getString("nombre"));
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error");
			
		}
		
		
	}
	
	public static void getValues(String db, String table) {
		
	}

}


