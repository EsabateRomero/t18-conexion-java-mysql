package dto;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Articulos {
	
	private static Connection conexion;
	private static String tabla = "Articulos";
	private static String bd;
	
	private int codigo = 0;
	private String nombre = "";
	private int precio;
	private int fabricante;
	
	public Articulos(Connection conexion, String bd) {
		this.conexion = conexion;
		this.bd = bd;
		createTable();
		
	}
	
	public Articulos(Connection conexion, String bd, String nombre, int precio) {
		this.conexion = conexion;
		this.bd = bd;
		createTable();
		this.nombre = nombre;
		crearArticulo(nombre, precio);
		
	}
	
	public Articulos(Connection conexion, String bd, String nombre, int precio, int fabricante) {
		this.conexion = conexion;
		this.bd = bd;
		createTable();
		this.nombre = nombre;
		crearArticulo(nombre, precio, fabricante);
		this.fabricante = fabricante;
	}
	
	public Articulos(Connection conexion, String bd, String nombre) {
		this.conexion = conexion;
		this.bd = bd;
		createTable();
		this.nombre = nombre;
		crearArticulo(nombre);
	}
	
	public static void createTable() {
	
		try {
			String Querydb = "USE " + bd + ";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE "+ tabla +
					" (codigo INT PRIMARY KEY AUTO_INCREMENT, nombre VARCHAR(50), "
					+ "precio INT, fabricante INT,"
					+ " FOREIGN KEY(fabricante) REFERENCES Fabricantes(codigo));";
			
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Taula creada");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("Error creando tabla");
		}
		
		
	}
	
	
	public static void crearArticulo(String nombre) {
		System.out.println("Articulos.crearArticulo()nombre");
		try {
		String Querydb = "USE "+bd+";";
		Statement stdb = conexion.createStatement();
		stdb.executeUpdate(Querydb);
		
		String Query = "INSERT INTO " + tabla + " (nombre) VALUE ("
				+ "\" "+ nombre +"\");";
		
		Statement st = conexion.createStatement();
		st.execute(Query);
		
		System.out.println("Datos introducidos");
		} catch (SQLException e) {
			System.out.println("No s'ha pogut introduir");
		}
	}
	
public static void crearArticulo(String nombre, int precio) {
		System.out.println("Articulos.crearArticulo()nombreprecio");
		try {
		String Querydb = "USE "+bd+";";
		Statement stdb = conexion.createStatement();
		stdb.executeUpdate(Querydb);
		
		String Query = "INSERT INTO " + tabla + " (nombre, precio) VALUE ("
				+ "\" "+ nombre +"\",\" "+precio+"\");";
		
		Statement st = conexion.createStatement();
		st.execute(Query);
		
		System.out.println("Datos introducidos");
		} catch (SQLException e) {
			System.out.println("No s'ha pogut introduir");
		}
	}

public static void crearArticulo(String nombre, int precio, int fabricante) {
	System.out.println("Articulos.crearArticulo()nombrepreciofabricante");
	try {
	String Querydb = "USE "+bd+";";
	Statement stdb = conexion.createStatement();
	stdb.executeUpdate(Querydb);
	
	String Query = "INSERT INTO " + tabla + " (nombre, precio, fabricante) VALUE ("
			+ "\" "+ nombre +"\",\" "+precio+"\",\" "+fabricante+"\");";
	
	Statement st = conexion.createStatement();
	st.execute(Query);
	
	System.out.println("Datos introducidos");
	} catch (SQLException e) {
		System.out.println("No s'ha pogut introduir");
	}
}
	
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public static void obtenerArticulos() {
		
		try {
			String Querydb = "USE "+ bd +";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "SELECT * FROM " + tabla;
			Statement st = conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
			
			while(resultSet.next()) {
				System.out.println("-------------\nCodigo:	" + resultSet.getInt("codigo") +"\nNombre:" + resultSet.getString("nombre")+"\nPrecio:" + resultSet.getInt("precio") +"\nFabricante:" + resultSet.getInt("fabricante"));
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error");
			
		}
		
		
	}
	
	public static void getValues(String db, String table) {
		
	}

}


