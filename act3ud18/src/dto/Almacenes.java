package dto;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Almacenes {
	
	private static Connection conexion;
	private static String tabla = "Almacenes";
	private static String bd;
	
	private int codigo = 0;
	private String lugar = "";
	private int capacidad = 0;
	
	
	//CONSTRUCTORES
	public Almacenes(Connection conexion, String bd) {
		this.conexion = conexion;
		this.bd = bd;
		createTable();
	}
	
	public Almacenes(Connection conexion, String bd, String lugar, int capacidad) {
		this.conexion = conexion;
		this.bd = bd;
		createTable();
		this.lugar = lugar;
		this.capacidad = capacidad;
		crearAlmacen(lugar, capacidad);	
		
	}
	
	
	//CREACI� TAULA
	public static void createTable() {
	
		try {
			String Querydb = "USE " + bd + ";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE IF NOT EXISTS "+ tabla +
					" (codigo INT PRIMARY KEY AUTO_INCREMENT, lugar NVARCHAR(100), "
					+ "capacidad INT);";
			
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("Error creando tabla");
		}
		
	}
	
	
	//INSERCI� DE EMPLEATS
	public static void crearAlmacen(String lugar, int capacidad) {
		System.out.println("Almacen a crear con:\n - Lugar: " + lugar + "\n - Capacidad: "+capacidad);
			try {
				String Querydb = "USE "+bd+";";
				Statement stdb = conexion.createStatement();
				stdb.executeUpdate(Querydb);
				
				String Query = "INSERT INTO " + tabla + " (lugar, capacidad) VALUE (\""+lugar+"\",\""+capacidad+"\");";
				
				Statement st = conexion.createStatement();
				st.execute(Query);
				
				System.out.println("	--> Datos introducidos correctamente\n");
			} catch (SQLException e) {
				System.out.println("No s'ha pogut introduir");
			}
	}
	
	
	//OBTENER ARTICUO
	public static void obtenerAlmacenes() {
		
		try {
			String Querydb = "USE "+ bd +";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "SELECT * FROM " + tabla;
			Statement st = conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
			
			while(resultSet.next()) {
				System.out.println("-------------\nCodigo:	" + resultSet.getInt("codigo") +"\nLugar: " + resultSet.getString("lugar")+"\nCapacidad: " + resultSet.getString("capacidad"));
			}			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error");
		}		
	}
	
	

	
	

	
	

}


