package dto;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Cajas {
	
	private static Connection conexion;
	private static String tabla = "Cajas";
	private static String bd;
	
	private int codigo = 0;
	private String contenido = "";
	private int valor = 0;
	private int almacen = 0;
	
	
	//CONSTRUCTORES
	public Cajas(Connection conexion, String bd) {
		this.conexion = conexion;
		this.bd = bd;
		createTable();
	}
	
	public Cajas(Connection conexion, String bd, String contenido, int valor, int almacen) {
		this.conexion = conexion;
		this.bd = bd;
		createTable();
		this.contenido = contenido;
		this.valor = valor;
		this.almacen = almacen;
		crearCaja(contenido, valor, almacen);	
	}
	

	
	//CREACI� TAULA
	public static void createTable() {
	
		try {
			String Querydb = "USE " + bd + ";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE IF NOT EXISTS "+ tabla +
					" (codigo INT PRIMARY KEY AUTO_INCREMENT, contenido NVARCHAR(100), "
					+ "valor INT, almacen INT,"
					+ " FOREIGN KEY(almacen) REFERENCES Almacenes(codigo));";
			
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("Error creando tabla");
		}
		
	}
	
	
	//INSERCI� DE CAJAS
	public static void crearCaja(String contenido, int valor, int almacen) {
		System.out.println("\n\nCaja a crear con:\n - Contenido: " + contenido + "\n - Valor: "+valor+ "\n - Almacen: "+almacen);
			try {
				String Querydb = "USE "+bd+";";
				Statement stdb = conexion.createStatement();
				stdb.executeUpdate(Querydb);
				
				String Query = "INSERT INTO " + tabla + " (contenido, valor, almacen) VALUE ("
						+ "\" "+ contenido +"\", "+valor+", "+almacen+");";
				
				Statement st = conexion.createStatement();
				st.execute(Query);
				System.out.println("	--> Datos introducidos correctamente\n");
			} catch (SQLException e) {
			
				System.out.println("No s'ha pogut introduir");
			}
	}

	
	
	//OBTENER CAJA
	public static void obtenerCajas() {
		
		try {
			String Querydb = "USE "+ bd +";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "SELECT * FROM " + tabla;
			Statement st = conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
			
			while(resultSet.next()) {
				System.out.println("-------------\nCodigo:	" + resultSet.getInt("codigo") +"\nContenido:" + resultSet.getString("contenido")+"\nValor:" + resultSet.getInt("valor") +"\nAlmacen:" + resultSet.getInt("almacen"));
			}			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error");
		}		
	}
	
	


	
}


