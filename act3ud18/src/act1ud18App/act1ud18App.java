package act1ud18App;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.InputMismatchException;
import java.util.Scanner;

import javax.swing.JOptionPane;

import dto.Almacenes;
import dto.Cajas;
import dto.Almacenes;


public class act1ud18App {

	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		final String bd = "act3";
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection conexion = DriverManager.getConnection("jdbc:mysql://192.168.1.134:3306?"
					+ "useTimezone=true&serverTimezone=UTC","remote","12345Remote<3!");
			System.out.println("Bienvenido a los almacenes\n  ----------------------- ");
			
			eliminarDB(bd, conexion);
			createDB(bd, conexion); 
			
			System.out.println("----------- ALMACENES -----------");
			Almacenes almacen = new Almacenes(conexion, bd, "Tarragona", 120);
			Almacenes almacen2 = new Almacenes(conexion, bd, "Barcelona", 300);
			Almacenes almacen3 = new Almacenes(conexion, bd, "Reus", 150);
			Almacenes almacen4 = new Almacenes(conexion, bd, "La Selva del Camp", 100);
			Almacenes almacen5 = new Almacenes(conexion, bd, "Valls", 130);
			
			
			System.out.println("--------------------------------------\n\nALMACENES REGISTRADOS: ");
			almacen.obtenerAlmacenes();
			
			System.out.println("\n\n------------ CAJAS ---------------");
			Cajas caja = new Cajas(conexion, bd, "fideos", 30, 1);
			Cajas caja2 = new Cajas(conexion, bd, "pezcado", 130, 2);
			Cajas caja3 = new Cajas(conexion, bd, "macarrones", 20, 3);
			Cajas caja4 = new Cajas(conexion, bd, "lapices", 36, 3);
			Cajas caja5 = new Cajas(conexion, bd, "botellas", 100, 4);
			
			System.out.println("--------------------------------------\n\nCAJAS REGISTRADAS: ");
			caja.obtenerCajas();
			
		} catch (SQLException | ClassNotFoundException e) {
			System.out.println("No se ha podido conectar con la base de datos");
			System.out.println(e);
		}

	}
	
	

	// CREACI� DE LA BASE DE DADES
	public static void eliminarDB(String name, Connection conexion) {
		try {
			String Query = "DROP DATABASE " + name;
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
		} catch (SQLException ex) {
		
		}
	}

	
	// CREACI� DE LA BASE DE DADES
	public static void createDB(String name, Connection conexion) {
		try {
			String Query = "CREATE DATABASE IF NOT EXISTS " + name;
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
		} catch (SQLException ex) {
			System.out.println("No se ha podido crear la base de datos");
		}
	}


	
	

}
